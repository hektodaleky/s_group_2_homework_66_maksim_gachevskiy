import React, {Component, Fragment} from "react";
import "./App.css";
import {NavLink, Route, Switch} from "react-router-dom";
import Elements from "./container/Elements/Elements";
import Home from "./container/Home/Home";
import ErrorBoundary from "./component/UI/ErrorBoundary/ErrorBoundary";

class App extends Component {
    render() {
        return (
            <ErrorBoundary>
                <Fragment>
                    <ul className="link">
                        <li><NavLink className="nav" to="/quotes">Quotes</NavLink></li>
                        <li><NavLink className="nav" to="/home">Home</NavLink></li>
                    </ul>

                    <Switch>
                        <Route path="/" exact render={() => <h1>Select menu</h1>}/>
                        <Route path="/quotes" component={Elements}/>
                        <Route path="/home" component={Home}/>
                        <Route render={() => <h1>Not found</h1>}/>

                    </Switch>

                </Fragment>
            </ErrorBoundary>
        );
    }
}

export default App;
