import React from "react";
import axios from "../../axios-orders";
import "./Elements.css";
import withAxiosComponent from "../../hoc/withAxiosComponent";
const Element = props => {


    return ( <div className="element">
            <h4>{props.author}</h4>
            <p>{props.categoryOption}</p>
            <p>{props.textVal}</p>
        </div>
    )


};
export default withAxiosComponent(Element,axios, '/quotes.json');