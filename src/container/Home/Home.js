import React from 'react';
import withAxiosComponent from "../../hoc/withAxiosComponent";
import axios from '../../axios-home';
import './Home.css';
const Home=props=>{
    return (
        <div className="Home">
            <h1>{props.title}</h1>
            <p>{props.text}</p>

        </div>
    )
};

export default withAxiosComponent(Home,axios, '/other.json');