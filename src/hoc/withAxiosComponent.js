import React, {Component, Fragment} from "react";

import Spinner from "../component/UI/Spinner/Spinner";
import ErrorBoundary from "../component/UI/ErrorBoundary/ErrorBoundary";
//URL можно спрятать в axios
const withAxiosComponent = (AxiosComponent, axios, url) => {
    return class extends Component {
        constructor(props) {
            super(props);

            this.state.id = axios.interceptors.response.use(res => res, error => {
                this.setState({error: error})
            });


        }

        state = {
            data: [],
            loaded: false,
            loading: false
        };

        componentWillUnmount() {
            console.log("Unmount")
        };

        componentDidMount() {
            this.loadItems(url);
        }

        loadItems = (url) => {
            this.setState({loading: true});

            axios.get(url).then(response => {
                let data = [];
                console.log(response.data);

                for (let key in response.data) {
                    data.push({...response.data[key], key})
                }
                console.log("Load item");
                this.setState({loaded: true, loading: false, data});

            }).catch(response => {
                this.setState({loading: false});
                console.log(response + " error in withAxiosComponent");
            })
        };

        render() {
            const {data, loading, loaded} = this.state;


            return (loading) ? <Spinner/> : (<ErrorBoundary><Fragment>
                {data.map((item, id) => {
                    // Для провертки ErrorBoundary
                    // if(Math.random() > 0.7)
                    // {throw new Error('Well, this happened.')};
                    return <AxiosComponent {...item}/>


                })}
            </Fragment></ErrorBoundary>)
        }
    }
};
export default withAxiosComponent;